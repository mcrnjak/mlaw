<?php
function showBattle($game){

	echo "<h3> Weather : ".$game->getWeather()."</h3>";
	echo "<h3> Time : ".$game->getTime()."</h3>";
	echo "<h4> Starting stats army1</h4>";
	$game->army1->showFullStats();
	echo "<h4> Starting stats army2</h4>";
	$game->army2->showFullStats();

	echo "<hr /><h3> Adding time factor</h3>";
	$game->applyTime();

	echo "<h4> New stats army1</h4>";
	$game->army1->showShortStats();
	echo "<h4> New stats army2</h4>";
	$game->army2->showShortStats();

	echo "<hr /><h3> Adding time weather</h3>";
	$game->applyWeather();

	echo "<h4> New stats army1</h4>";
	$game->army1->showShortStats();
	echo "<h4> New stats army2</h4>";
	$game->army2->showShortStats();

	echo "<hr /><h3> Adding army type factor</h3>";
	$game->applyType();

	echo "<h4> New stats army1</h4>";
	$game->army1->showShortStats();
	echo "<h4> New stats army2</h4><hr />";
	$game->army2->showShortStats();

	if ($game->getResult() == 1){
	echo "<h1> Pobijedila je prva vojska! </h3>";
	
	}else{
		echo "<h1> Pobijedila je druga vojska! </h3>";
	}

}
?>