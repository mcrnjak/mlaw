<?php 
function myAutoloader($class_name) {
    include 'classes/' . $class_name . '.inc.php';
}
spl_autoload_register('myAutoloader');
?>