<!--
Zadatak je sljedeći:

Napraviti mini borbu dvije vojske koja se sastoji samo od programerskog dijela u PHPu bez vidljivog sučelja. 
Igra je tekstualni "rat" dvije vojske. Vojske su sačinjene od N vojnika, N se dobiva iz $_GET-a sa ?army1=50&army2=48. 
Logiku borbe, strukturu vojske, tipove podataka, strukturu klasa, strukturu fileova, ispise ako ih ima, sve dodatne feature i opcije smišljaš sam. 
Jedna vlastita implementirana ideja je obavezna (može biti bilo što, generali, random potresi, vojnici polude, baš bilo što). 
Sve je dopušteno i u principu nema ograničenja koliko mali ili veliki cijeli program mora biti. Na kraju je bitno samo da se na neki način vidi koja vojska je pobijedila i zašto. 
-->

<?php

include 'functions/showBattle.php';
include 'functions/showError.php';
include 'functions/autoloader.php';

$game = new Game();

if($game->error){
	showError();
}else{
	showBattle($game);
}

?>