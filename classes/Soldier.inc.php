<?php  
/**
* Soldier - basic unit
* 
*/

class Soldier extends Unit{

	function __construct(){
		$this->type = 'Soldier';
		$this->generateBasePower();
		$this->generateStats();
		$this->calculatePower();
	}

	protected function generateBasePower(){
		$this->base_power = rand(80, 100);
	}
}

?>