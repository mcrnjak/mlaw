<?php  
/**
* Soldier class -povecaj broj vojnika za jedan 
* Postavi base power na 100 
* izracunaj power
* spremi vrijednosti
* 
*/

class General extends Unit{

	public $general_rank = 0;

	function __construct(){
		$this->type = 'General';
		$this->generateGeneralRank();
		$this->generateBasePower();
		$this->generateStats();
		$this->calculatePower();
		$this->teamworkBonus();
	}

	protected function generateGeneralRank(){
		$this->general_rank = rand(1, 10);
	}

	protected function generateBasePower(){
		$this->base_power = rand(80, 120)*$this->general_rank;
	}

	protected function teamworkBonus(){
		$this->teamwork_factor *= $this->general_rank;
	}

}

?>