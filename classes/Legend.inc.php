<?php  
/**
*Legend is godlike 
*max num in one army = 1
*/

class Legend extends Unit{
	
	function __construct(){
		$this->type = 'LEGEND';
		$this->generateBasePower();
		$this->generateStats();
		$this->teamworkClean();
		$this->calculatePower();
	}

	protected function generateBasePower(){
		$this->base_power = rand(8000, 10000);
	}

	protected function teamworkClean(){
		$this->teamwork_factor = 0;
	}
}
?>