<?php 

/**
* This is core class of all units
*/
abstract class Unit{
	protected $base_power = 100;
	protected $type = 'base_unit';

	//Next stats can take values 1-100
	protected $morale = 0;
	protected $luck = 0;
	protected $experience = 0;
	
	//Return variables
	protected $teamwork_factor = 0;
	protected $power = 0;

	protected function generateStats(){
		$this->morale = rand(1, 100);
		$this->luck = rand(1, 100);
		$this->experience = rand(1, 100);
		$this->teamwork_factor = rand(1, 100);
	}

	protected function printStats(){
		$output = '<br/ >';
		$output .= $this->type;
		$output .= '|| base_power: '.$this->base_power;
		$output .= '|| morale: '.$this->morale;
		$output .= '|| luck: '.$this->luck;
		$output .= '|| experience: '.$this->experience;
		$output .= '|| teamwork_factor: '.$this->teamwork_factor;
		$output .= '|| power: '.$this->power;
		$output .= '<hr>';
		echo $output;
	}

	protected function calculatePower(){
		$base = $this->base_power;
		$return = $base;

		$return += $base * $this->morale / 100;
		$return += $base * $this->luck / 100;
		$return += $base * $this->experience / 100;
		$this->power = round($return, 0, PHP_ROUND_HALF_DOWN);
	}

	public function getTeamwork(){
		return $this->teamwork_factor;
	}

	public function getPower(){
		return $this->power;
	}
}

?>