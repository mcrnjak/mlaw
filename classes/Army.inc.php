<?php 

class Army{

	public $army_power = 0;
	public $army_strategy = '';
	protected $army_teamwork = 0;
	
	protected $army_teamwork_sum = 0;
	protected $army_holder = [];
	protected $num_of_soldiers = 0;
	protected $num_of_generals = 0;
	protected $num_of_legends = 0;


	function __construct($number){
		$this->generateUnits($number);
		$this->generateStrategy();
		$this->calculateTeamwork();
		$this->applyTeamwork();
		// $this->showShortStats();
	}

	public function showFullStats(){
		$output = '';		
		$output .= 'Power: '. $this->army_power;
		$output .= '<br />Strategy: '. $this->army_strategy;
		
		$output .= '<br />Number of legends: '. $this->num_of_legends;
		$output .= '<br />Number of generals: '. $this->num_of_generals;
		$output .= '<br />Number of soldiers: '. $this->num_of_soldiers;
		$output .= '<br />';
		echo $output;
	}

	public function showShortStats(){
		$output = '';
		$output .= 'Power: '. $this->army_power;
		$output .= '<br />Strategy: '. $this->army_strategy;
		$output .= '<br />';
		echo $output;
	}

	protected function generateStrategy(){
		$roulette = rand(1,2);
		
		if ($roulette == 1){
			$this->army_strategy = 'magic';
		
		}elseif ($roulette == 2){
			$this->army_strategy = 'fighter';
	
		}else{
			$this->army_strategy = 'assasin';
		}

	}
	
	protected function generateUnits($number){
		for ($number; $number > 0 ; $number--){
			$roulette = rand(1, 100);		

			//1% to generate legend
			if ($roulette == 100 && $this->num_of_legends == 0){
				$this->num_of_legends = 1;
				$this->army_holder[$number] = new Legend;
			
			//5% to generate general 
			}else if($roulette <= 5 && $this->num_of_generals < 4){
				$this->num_of_generals++;
				$this->army_holder[$number] = new General;
			
			}else{
				$this->num_of_soldiers++;
				$this->army_holder[$number] = new Soldier;
			}

			//adding stats to army
			$this->addStats($this->army_holder[$number]);
		}
	}

	protected function addStats($unit){
		//increase teamwork summ
		$this->army_teamwork_sum += $unit->getTeamwork();
		
		//increase power summ
		$this->army_power += $unit->getPower();
	}

	protected function calculateTeamwork(){
		$this->army_teamwork = $this->army_teamwork_sum / ($this->num_of_soldiers + $this->num_of_generals);
	}

	protected function applyTeamwork(){
		$this->army_power += $this->army_power * $this->army_teamwork / 100;
		$this->roundPower();
	}
	public function roundPower(){
		$this->army_power = round($this->army_power, 0, PHP_ROUND_HALF_DOWN);
	}
}
?>