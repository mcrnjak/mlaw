
<?php 

/**
* Game class
*/
class Game{
    protected $size_first = 0;
    protected $size_second = 0;

    public $army1;
    public $army2;
    public $error = false;

    protected $time = '';
    protected $weather = '';

    function __construct(){
        $this->init();
        $this->generateTime();
        $this->generateWeather();
    }

    protected function init(){
        if (!isset($_GET['army1']) || !isset($_GET['army2'])){
            $this->getNotSet();
        }else{
            $this->setSizeFirst($_GET['army1']);
            $this->setSizeSecond($_GET['army2']);
            $this->army1 = new Army($this->size_first);
            $this->army2 = new Army($this->size_second);
        }
    }

    protected function setSizeFirst($size){
        $this->size_first = $size;
    }

    protected function setSizeSecond($size){
        $this->size_second = $size;
    }

    protected function getNotSet(){
        $this->error = true;
    } 

    protected function generateTime(){
        $roulette = rand(1, 2);
        if ($roulette == 1){
           $this->time = 'night';
           
        }elseif ($roulette == 2) {
           $this->time = 'morning';

        }else{
           $this->time = 'afternoon';

        }
    }

    protected function generateWeather(){
        $roulette = rand(1, 2);
        if ($roulette == 1){
           $this->weather = 'fog';
           
        }elseif ($roulette == 2) {
           $this->weather = 'sun';

        }else{
           $this->weather = 'rain';
           
        }
    }

    protected function increasePower($army){
        $army->army_power = $army->army_power * 1.20;
        $army->roundPower();
    }

    public function applyTime(){
        $this->calculateTime($this->army1);
        $this->calculateTime($this->army2);
    }

    protected function calculateTime($army){
        //magic works best in afternoon
        //fighter works best in morning
        //assasin works best in night

        $bonus = false;
        if($army->army_strategy == 'magic' && $this->time == 'afternoon'){
            $bonus = true;
        
        }elseif ($army->army_strategy == 'fighter' && $this->time == 'morning'){
            $bonus = true;
        
        }elseif ($army->army_strategy == 'assasin' && $this->time == 'night'){
            $bonus = true;
        }

        if ($bonus) {
            $this->increasePower($army);
            // $army->time_bonus = true;
        }

    }

    public function applyWeather(){
        $this->calculateWeather($this->army1);
        $this->calculateWeather($this->army2);
    }

    protected function calculateWeather($army){
        //magic works best in rain
        //fighter works best in sun
        //assasin works best in fog
        $bonus = false;
        if($army->army_strategy == 'magic' && $this->weather == 'rain'){
            $bonus = true;
        
        }elseif ($army->army_strategy == 'fighter' && $this->weather == 'sun'){
            $bonus = true;
        
        }elseif ($army->army_strategy == 'assasin' && $this->weather == 'fog'){
            $bonus = true;
        }

        if ($bonus) {
            $this->increasePower($army);
            // $army->weather_bonus = true;
        }

    }

    public function applyType(){
        //magic works best vs fighter
        //fighter works best vs assasin
        //assasin works best vs magic
        if($this->army1->army_strategy != $this->army2->army_strategy){
            if ($this->army1->army_strategy == 'magic'){
                
                if ($this->army2->army_strategy == 'fighter'){                    
                    $this->increasePower($this->army1);
                //assasin
                }else{
                    $this->increasePower($this->army2);
                }

            }elseif ($this->army1->army_strategy == 'fighter'){
                if ($this->army2->army_strategy == 'assasin'){                    
                    $this->increasePower($this->army1);

                //magic
                }else{
                    $this->increasePower($this->army2);
                }

            }elseif ($this->army1->army_strategy == 'assasin'){
                if ($this->army2->army_strategy == 'magic'){                    
                    $this->increasePower($this->army1);

                //fighter
                }else{
                    $this->increasePower($this->army2);
                }
            }
        }
    }

    public function getResult(){
        $ret_var = ($this->army1->army_power > $this->army2->army_power ? 1 : 2 );
        return $ret_var;
    }

    public function getWeather(){
        return $this->weather;
    }

    public function getTime(){
        return $this->time;
    }

}

?>